const listBuah = [
    {
        nama : "Mangga",
        rasa: "Manis"
    },
    {
        nama: "Pisang",
        rasa: "Manis"
    },
    {
        nama: "Jeruk",
        rasa: "Asam"
    },
    {
        nama: "Aplokat",
        rasa: "Manis"
    },
    {
        nama: "Coklat",
        rasa: "Pahit"
    },
]

for (let i = 0; i < listBuah.length; i++) {
    console.log(listBuah[i]["nama"],
        ("rasa " + listBuah[i]["rasa"]));
}

console.log("-cari-")
function cariBuah(nama) {
    for(let i = 0; i < listBuah.length; i++) {
        if (listBuah[i]["nama"] == nama) {
            console.log(listBuah[i]["nama"], ("rasa " + listBuah[i]["rasa"]));
        }
    }
}
cariBuah("Mangga");

console.log("-filter-");
function filterBuah(rasa) {
    for(let i = 0; i < listBuah.length; i++){
        if (listBuah[i]["rasa"] == rasa){
            console.log(listBuah[i]["nama"]);
        }
    }
}
filterBuah("Manis")